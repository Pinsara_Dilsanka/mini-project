package currencyConverter;

import java.awt.EventQueue;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.Color;
import java.awt.SystemColor;
import javax.swing.UIManager;
import javax.swing.text.JTextComponent;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class welcome_page{

	private JFrame frame;
	public JTextField textField;
	static public String username;
	static public String count;
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					welcome_page window = new welcome_page();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public welcome_page() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 456, 291);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Currency Converter");
		lblNewLabel.setForeground(new Color(173, 255, 47));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Times New Roman", Font.BOLD, 25));
		lblNewLabel.setBounds(58, 11, 289, 28);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Username");
		lblNewLabel_1.setForeground(new Color(189, 183, 107));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setFont(new Font("Times New Roman", Font.BOLD, 20));
		lblNewLabel_1.setBounds(58, 82, 138, 28);
		frame.getContentPane().add(lblNewLabel_1);
		
		textField = new JTextField();
		textField.setBounds(206, 84, 141, 28);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("Login");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					username = textField.getText();
					
					
					try {
						//Class.forName("com.mysql.jdbc.Driver");
						Connection	 connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "");//Establishing connection
						System.out.println("Connected With the database successfully");
						
						//Creating PreparedStatement object
						PreparedStatement preparedStatement=connection.prepareStatement("INSERT INTO users (User_Name) VALUES (?)");
						preparedStatement.setString(1,username); 						 
				         //Executing Query
				         preparedStatement.executeUpdate();
				         
				         PreparedStatement preparedStatement1=connection.prepareStatement("SELECT COUNT(User_Name) as uniqueVisitor FROM users WHERE User_Name=?");
				         preparedStatement1.setString(1,username);
				         ResultSet rs=preparedStatement1.executeQuery();
	   
					         while(rs.next()){
					        	 count =rs.getString("uniqueVisitor");
					        	 System.out.println("You have visited "+count+" times");
					         }
					        
					         
				         connection.close();
				       
				        MainWindow.main(null); 
							

					} catch (SQLException e1) {
						
						System.out.println(e1+"Error while connecting to the database");
					
							}

				}catch(Exception ex) {
					System.out.println("Error");
				}
			}
		});
		
		btnNewButton.setFont(new Font("Times New Roman", Font.BOLD, 16));
		btnNewButton.setBounds(58, 140, 101, 33);
		frame.getContentPane().add(btnNewButton);
		
		JButton btnNewButton_1 = new JButton("Exit");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton_1.setFont(new Font("Times New Roman", Font.BOLD, 18));
		btnNewButton_1.setBounds(252, 139, 101, 33);
		frame.getContentPane().add(btnNewButton_1);
		
		JLabel lblNewLabel_2 = new JLabel("|| currency converter || Sprinters group ||");
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(42, 207, 305, 19);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("All rights received");
		lblNewLabel_3.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_3.setBounds(81, 227, 215, 14);
		frame.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("");
		lblNewLabel_4.setIcon(new ImageIcon("C:\\Users\\file\\Desktop\\currency\\backgrounds\\page_background.jpg"));
		lblNewLabel_4.setBounds(0, 0, 440, 252);
		frame.getContentPane().add(lblNewLabel_4);
	}
}