package currencyConverter;

import java.awt.EventQueue;
import java.sql.*;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JComboBox;
import java.awt.Font;
import javax.swing.SwingConstants;
import javax.swing.DefaultComboBoxModel;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.ImageIcon;
import java.awt.Color;

public class MainWindow extends welcome_page{

	private JFrame frame;
	private JTextField textField_2;
	protected Object lblNewLabel_2;
	private static DecimalFormat df2 = new DecimalFormat("#.####");


	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	
		
	
	}
	

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}
	/**
	 * Initialize the contents of the frame.
	 */
	void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 503, 335);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Convert From");
		lblNewLabel.setForeground(new Color(173, 255, 47));
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel.setFont(new Font("Times CG ATT", Font.BOLD, 18));
		lblNewLabel.setBounds(38, 62, 124, 27);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Convert to");
		lblNewLabel_1.setForeground(new Color(173, 255, 47));
		lblNewLabel_1.setFont(new Font("Times CG ATT", Font.BOLD, 18));
		lblNewLabel_1.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_1.setBounds(38, 97, 124, 28);
		frame.getContentPane().add(lblNewLabel_1);
		
		JComboBox<Object> comboBox_1 = new JComboBox<Object>();
		comboBox_1.setModel(new DefaultComboBoxModel<Object>(new String[] {"USD", "LKR", "INR", "GBP", "SGD", "JPY"}));
		comboBox_1.setToolTipText("");
		comboBox_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		comboBox_1.setBounds(193, 63, 130, 25);
		frame.getContentPane().add(comboBox_1);
		
		JComboBox<Object> comboBox_2 = new JComboBox<Object>();
		comboBox_2.setFont(new Font("Tahoma", Font.PLAIN, 15));
		comboBox_2.setModel(new DefaultComboBoxModel<Object>(new String[] {"USD", "LKR", "INR", "GBP", "SGD", "JPY"}));
		comboBox_2.setToolTipText("USD\r\nLKR\r\nEuro\r\nGBP\r\nINR\r\nYen");
		comboBox_2.setBounds(193, 99, 130, 25);
		frame.getContentPane().add(comboBox_2);
		
		JLabel lblNewLabel_5 = new JLabel("value here");
		lblNewLabel_5.setForeground(new Color(255, 215, 0));
		lblNewLabel_5.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		lblNewLabel_5.setBounds(26, 228, 386, 21);
		frame.getContentPane().add(lblNewLabel_5);
		
		JButton btnNewButton = new JButton("Convert");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				
				try {
					//Class.forName("com.mysql.jdbc.Driver");
					Connection	 connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test", "root", "");//Establishing connection
					//Creating PreparedStatement object
					PreparedStatement preparedStatement=connection.prepareStatement("SELECT currencyRate from currencyrates;");
					 						 
			         //Executing Query
					 ResultSet rs=preparedStatement.executeQuery();
			         
			          
			        List<Integer> list = new ArrayList<Integer>();
			         while(rs.next()){
			             Integer value=rs.getInt("currencyRate");
			             list.add(value);
			         }  
				        
			         	int value_USD = list.get(0);
			          	int value_LKR = list.get(1);
				        int value_INR = list.get(2);
				        int value_GBP = list.get(3);
				        int value_SGD = list.get(4);
				        int value_JPY = list.get(5);
				        String userInput= textField_2.getText();
				        double C1 = 0;
				        
				        
//				       if (userInput== "")
//				       JOptionPane.showMessageDialog(btnNewButton, "Insert a Value!!!");
				        
				        if (comboBox_1.getSelectedItem().equals("USD") && comboBox_2.getSelectedItem().equals("LKR")  ) {
				        	C1 = (Double.parseDouble(userInput)* value_LKR);
				        	lblNewLabel_5.setText(userInput+" USD equals to "+df2.format(C1)+" Sri Lankan Rupees ");
				        }				       				    
				        
				        	else if (comboBox_1.getSelectedItem().equals("USD") && comboBox_2.getSelectedItem().equals("INR")) { 
				        		C1 = (Double.parseDouble(userInput)* value_INR);
				        	lblNewLabel_5.setText(userInput+" US Dollars equals to "+df2.format(C1)+" Indian Rupees ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("USD") && comboBox_2.getSelectedItem().equals("GBP")) { 
				        		C1 = (Double.parseDouble(userInput)* value_GBP);
				        	lblNewLabel_5.setText(userInput+" US Dollars equals to "+df2.format(C1)+" British Pounds ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("USD") && comboBox_2.getSelectedItem().equals("SGD")) { 
				        		C1 = (Double.parseDouble(userInput)* value_SGD);
				        	lblNewLabel_5.setText(userInput+" US Dollars equals to "+df2.format(C1)+" Singapore Dollars ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("USD") && comboBox_2.getSelectedItem().equals("JPY")) { 
				        		C1 = (Double.parseDouble(userInput)* value_JPY);
				        	lblNewLabel_5.setText(userInput+" US Dollars equals to "+df2.format(C1)+" Japanese Yen ");

				        	}
				        
				        
				        if (comboBox_1.getSelectedItem().equals("LKR") && comboBox_2.getSelectedItem().equals("USD")  ) {
				        	C1 = (Double.parseDouble(userInput)* value_USD/value_LKR);
				        	lblNewLabel_5.setText(userInput+" LKR equals to "+df2.format(C1)+" US Dollars ");
				        }				       				    
				        
				        	else if (comboBox_1.getSelectedItem().equals("LKR") && comboBox_2.getSelectedItem().equals("INR")) { 
				        		C1 = (Double.parseDouble(userInput)*value_INR/value_LKR);
				        	lblNewLabel_5.setText(userInput+" LKR equals to "+df2.format(C1)+" Indian Rupees ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("LKR") && comboBox_2.getSelectedItem().equals("GBP")) { 
				        		C1 = (Double.parseDouble(userInput)*value_GBP/value_LKR);
				        	lblNewLabel_5.setText(userInput+" LKR equals to "+df2.format(C1)+" British Pounds ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("LKR") && comboBox_2.getSelectedItem().equals("SGD")) { 
				        		C1 = (Double.parseDouble(userInput)*value_SGD/value_LKR);
				        	lblNewLabel_5.setText(userInput+" LKR equals to "+df2.format(C1)+" Singapore Dollars ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("LKR") && comboBox_2.getSelectedItem().equals("JPY")) { 
				        		C1 = (Double.parseDouble(userInput)*value_JPY/value_LKR);
				        	lblNewLabel_5.setText(userInput+" LKR equals to "+df2.format(C1)+" Japanese Yen ");

				        	}
				        
				        if (comboBox_1.getSelectedItem().equals("INR") && comboBox_2.getSelectedItem().equals("USD")  ) {
				        	C1 = (Double.parseDouble(userInput)*value_USD/value_INR);
				        	lblNewLabel_5.setText(userInput+" INR equals to "+df2.format(C1)+" US Dollars ");
				        }				       				    
				        
				        	else if (comboBox_1.getSelectedItem().equals("INR") && comboBox_2.getSelectedItem().equals("LKR")) { 
				        		C1 = (Double.parseDouble(userInput)*value_LKR/value_INR);
				        	lblNewLabel_5.setText(userInput+" INR equals to "+df2.format(C1)+" Sri Lankan Rupees ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("INR") && comboBox_2.getSelectedItem().equals("GBP")) { 
				        		C1 = (Double.parseDouble(userInput)*value_GBP/value_INR);
				        	lblNewLabel_5.setText(userInput+" INR equals to "+df2.format(C1)+" British Pounds ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("INR") && comboBox_2.getSelectedItem().equals("SGD")) { 
				        		C1 = (Double.parseDouble(userInput)*value_SGD/value_INR);
				        	lblNewLabel_5.setText(userInput+" INR equals to "+df2.format(C1)+" Singapore Dollars ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("INR") && comboBox_2.getSelectedItem().equals("JPY")) { 
				        		C1 = (Double.parseDouble(userInput)*value_JPY/value_INR);
				        	lblNewLabel_5.setText(userInput+" INR equals to "+df2.format(C1)+" Japanese Yen ");

				        	}
				        
				        if (comboBox_1.getSelectedItem().equals("GBP") && comboBox_2.getSelectedItem().equals("USD")  ) {
				        	C1 = (Double.parseDouble(userInput)*value_USD/value_GBP);
				        	lblNewLabel_5.setText(userInput+" GBP equals to "+df2.format(C1)+" US Dollars ");
				        }				       				    
				        
				        	else if (comboBox_1.getSelectedItem().equals("GBP") && comboBox_2.getSelectedItem().equals("LKR")) { 
				        		C1 = (Double.parseDouble(userInput)*value_LKR/value_GBP);
				        	lblNewLabel_5.setText(userInput+" GBP equals to "+df2.format(C1)+" Sri Lankan Rupees ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("GBP") && comboBox_2.getSelectedItem().equals("INR")) { 
				        		C1 = (Double.parseDouble(userInput)*value_INR/value_GBP);
				        	lblNewLabel_5.setText(userInput+" GBP equals to "+df2.format(C1)+" Indian Rupees ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("GBP") && comboBox_2.getSelectedItem().equals("SGD")) { 
				        		C1 = (Double.parseDouble(userInput)*value_SGD/value_GBP);
				        	lblNewLabel_5.setText(userInput+" GBP equals to "+df2.format(C1)+" Singapore Dollars ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("GBP") && comboBox_2.getSelectedItem().equals("JPY")) { 
				        		C1 = (Double.parseDouble(userInput)*value_JPY/value_GBP);
				        	lblNewLabel_5.setText(userInput+" GBP equals to "+df2.format(C1)+" Japanese Yen ");

				        	}
				        
				        if (comboBox_1.getSelectedItem().equals("SGD") && comboBox_2.getSelectedItem().equals("USD")  ) {
				        	C1 = (Double.parseDouble(userInput)*value_USD/value_SGD);
				        	lblNewLabel_5.setText(userInput+" SGD equals to "+df2.format(C1)+" US Dollars ");
				        }				       				    
				        
				        	else if (comboBox_1.getSelectedItem().equals("SGD") && comboBox_2.getSelectedItem().equals("LKR")) { 
				        		C1 = (Double.parseDouble(userInput)*value_LKR/value_SGD);
				        	lblNewLabel_5.setText(userInput+" SGD equals to "+df2.format(C1)+" Sri Lankan Rupees ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("SGD") && comboBox_2.getSelectedItem().equals("INR")) { 
				        		C1 = (Double.parseDouble(userInput)*value_INR/value_SGD);
				        	lblNewLabel_5.setText(userInput+" SGD equals to "+df2.format(C1)+" Indian Rupees ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("SGD") && comboBox_2.getSelectedItem().equals("GBP")) { 
				        		C1 = (Double.parseDouble(userInput)*value_GBP/value_SGD);
				        	lblNewLabel_5.setText(userInput+" SGD equals to "+df2.format(C1)+" British Pounds ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("SGD") && comboBox_2.getSelectedItem().equals("JPY")) { 
				        		C1 = (Double.parseDouble(userInput)*value_JPY/value_SGD);
				        	lblNewLabel_5.setText(userInput+" SGD equals to "+df2.format(C1)+" Japanese Yen ");

				        	}
				        
				        if (comboBox_1.getSelectedItem().equals("JPY") && comboBox_2.getSelectedItem().equals("USD")  ) {
				        	C1 = (Double.parseDouble(userInput)*value_USD/value_JPY);
				        	lblNewLabel_5.setText(userInput+" JPY equals to "+df2.format(C1)+" US Dollars ");
				        }				       				    
				        
				        	else if (comboBox_1.getSelectedItem().equals("JPY") && comboBox_2.getSelectedItem().equals("LKR")) { 
				        		C1 = (Double.parseDouble(userInput)*value_LKR/value_JPY);
				        	lblNewLabel_5.setText(userInput+" JPY equals to "+df2.format(C1)+" Sri Lankan Rupees ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("JPY") && comboBox_2.getSelectedItem().equals("INR")) { 
				        		C1 = (Double.parseDouble(userInput)*value_INR/value_JPY);
				        	lblNewLabel_5.setText(userInput+" JPY equals to "+df2.format(C1)+" Indian Rupees ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("JPY") && comboBox_2.getSelectedItem().equals("GBP")) { 
				        		C1 = (Double.parseDouble(userInput)*value_GBP/value_JPY);
				        	lblNewLabel_5.setText(userInput+" JPY equals to "+df2.format(C1)+" British Pounds ");

				        	}
				        
				        	else if (comboBox_1.getSelectedItem().equals("JPY") && comboBox_2.getSelectedItem().equals("SGD")) { 
				        		C1 = (Double.parseDouble(userInput)*value_SGD/value_JPY);
				        	lblNewLabel_5.setText(userInput+" JPY equals to "+df2.format(C1)+" Singapore Dollars ");

				        	}


				       
			         
			         connection.close();
			         //MainWindow.main(null); 
	
				} catch (SQLException e1) {
					
					System.out.println(e1+"Error while connecting to the database");
				
						}
				
				
				
				
			}
				
			
		});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 18));
		btnNewButton.setBounds(143, 185, 130, 35);
		frame.getContentPane().add(btnNewButton);

		JLabel lblNewLabel_2 = new JLabel("Hey "+ username+" !!!");
		lblNewLabel_2.setForeground(new Color(127, 255, 0));
		lblNewLabel_2.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(150, 26, 154, 25);
		frame.getContentPane().add(lblNewLabel_2);
		
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setBounds(172, 0, 118, 25);
		frame.getContentPane().add(lblNewLabel_3);
		
		JLabel lblNewLabel_4 = new JLabel("Amount");
		lblNewLabel_4.setForeground(new Color(173, 255, 47));
		lblNewLabel_4.setFont(new Font("Times CG ATT", Font.BOLD, 18));
		lblNewLabel_4.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_4.setBounds(38, 128, 110, 35);
		frame.getContentPane().add(lblNewLabel_4);
		
		textField_2 = new JTextField();
		textField_2.setBounds(193, 135, 130, 25);
		frame.getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 436, 22);
		frame.getContentPane().add(menuBar);
		
		JMenu fileMenu = new JMenu("File");    
		menuBar.add(fileMenu);
		
		JMenuItem quitMenuItem = new JMenuItem("Quit");
		quitMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		fileMenu.add(quitMenuItem);
		
		JMenu adminMenu = new JMenu("Admin");    
		menuBar.add(adminMenu);
		
		JMenuItem changeratesItem = new JMenuItem("Change Rates");
		changeratesItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				 Admin_page.main(null);
			}
		});
		adminMenu.add(changeratesItem);
		
		JMenu aboutMenu = new JMenu("About");    
		menuBar.add(aboutMenu);
		
		JMenuItem aboutusItem = new JMenuItem("About Us");
		aboutusItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				aboutUs.main(null);
			}
		});
		aboutMenu.add(aboutusItem);
		
		JLabel lblNewLabel_10 = new JLabel("Visitor Count : "+count);
		lblNewLabel_10.setForeground(new Color(0, 0, 0));
		lblNewLabel_10.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_10.setFont(new Font("Tahoma", Font.PLAIN, 16));
		lblNewLabel_10.setBounds(302, 247, 154, 25);
		frame.getContentPane().add(lblNewLabel_10);
		
		JLabel lblNewLabel_6 = new JLabel("");
		lblNewLabel_6.setIcon(new ImageIcon("C:\\Users\\Pradeepa\\eclipse-workspace\\currencyConverter\\backgrounds\\page_background.jpg"));
		lblNewLabel_6.setBounds(0, 23, 487, 273);
		frame.getContentPane().add(lblNewLabel_6);
		
		
		
		
	}
}
